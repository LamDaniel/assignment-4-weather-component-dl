"use strict"; //always

/**
 * Weather class is a React Component that sets up the API and displays the weather components when rendered.
 * When mounted, it fetches from API first. If it's a valid response, takes data from JSON and renders a section to display data. If not, displays error message.
 * @author Daniel Lam 1932789
 */
class Weather extends React.Component {

    /**
     * Constructor for Weather class. Takes in as parameters props values when initialized.
     * Sets up state with default variables too.
     * @param {*} props data provided for URL query
     */
    constructor(props) {
        super(props);
        this.state = {
            temp: 0,
            condition: '',
            img: ''
        };
    }

    /**
     * componentDidMount() is a method that gets invoked when the the Weather component gets rendered.
     * It creates a URL based on the API and props provided and fetches from API using this URL.
     */
    componentDidMount() {
        //No rights reserved. API provided by: 'https://openweathermap.org/current'
        let url = "http://api.openweathermap.org/data/2.5/weather?";
        const data = {
            q: this.props.city,
            appid: this.props.idkey
        };
        let params = new URLSearchParams(data);
        url = url + params.toString();
        this.fetchAPI(url);
    }

    /**
     * fetchAPI() is a method that fetches from API using full URL provided by componentDidMount().
     * If API returns a good response, returns a JSON and invokes displayWeather() using this JSON as its parameters. If not, invokes displayError to display error message.
     * @param {*} url full URL with query
     */
    fetchAPI(url) {
        fetch(url) 
        .then (response => {
            if (response.ok) {
                return response.json(); //successful: return the json and chains to next .then()
            }
            throw new Error("ERROR: API Status code: " + response.status); 
        })
        .then (json => this.displayWeather(json))
        .catch(error => this.displayError(error))
    }

    /**
     * displayWeather() is a method that sets the state of the Weather component based on the JSON returned from fetchAPI().
     * It sets the temperature in celsius, the Weather description and the Weather icon for the Weather component to render.
     * @param {*} json JSON from API
     */
    displayWeather(json) {
        this.setState( {
            temp: Math.round((json.main.temp - 273.15) * 100)/100,
            condition: json.weather[0].description.charAt(0).toUpperCase() + json.weather[0].description.slice(1),
            img: json.weather[0].icon
        });
    }

    /**
     * displayError() is a method that gets invoked when API has encountered an error.
     * It displays an error message on the console and on the website.
     * @param {*} error 
     */
    displayError(error) {
        console.log(error);
        global.main.style.color = "red";
        global.main.textContent = "Weather is not available at this moment. Try again."
        this.setState( {
            temp: -1,
            condition: "Weather is not available at this moment. Try again.",
            img: ''
        });
    }

    /**
     * render() is a method that displays the Weather components on the website.
     * It takes the values from the state of the component after fetching from API and returns a section that contains these elements.
     * @returns section Section holding all the Weather components.
     */
    render() {
        //No rights reserved. All pictures belongs to their respective owner on 'http://openweathermap.org/img/wn/@2x.png'
        let imgsrc = 'http://openweathermap.org/img/wn/' + this.state.img + '@2x.png';
        return (
            <section id='container'>
                <h1>Weather Forecast for Today:</h1>
                <div id="weather">
                    <p>{this.state.temp}°C {this.state.condition}</p>
                    <img src={imgsrc} alt="Weather Icon"></img>
                    <button tabindex='0' onClick={() => this.componentDidMount()}>Refresh</button>
                </div>
            </section>
        );
    }
}

//Variables
let global = {}; //namespace
const id = "29e66bfd2def72a5bca64ae596012d58"; //API id key (saved as variable)
global.main = document.querySelector("#main");

//Render Weather component with city and idkey as props
ReactDOM.render(<Weather city='Montreal' idkey={id}/>, global.main);